package com.github.aecsocket.orphea.player

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.ResultReceiver
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.media.AudioAttributesCompat
import androidx.media.AudioFocusRequestCompat
import androidx.media.AudioManagerCompat
import coil.request.ImageRequest
import com.github.aecsocket.orphea.App
import com.github.aecsocket.orphea.TAG
import com.github.aecsocket.orphea.media.StreamData
import com.google.android.exoplayer2.PlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import org.schabi.newpipe.extractor.stream.StreamType

const val STATE_FETCHING = 0
const val STATE_PAUSED = 1
const val STATE_PLAYING = 2

const val DURATION_UNKNOWN = -1L

const val VOLUME_DUCK = 0.2f
const val VOLUME_DUCK_TIME = 500L

const val RESTART_THRESHOLD = 1000L
const val PROGRESS_UPDATE_PERIOD = 10L

sealed class ActiveStreamData(
    val data: StreamData,
    val artRequest: ImageRequest.Builder?
)

class FetchingStreamData(
    data: StreamData,
    artRequest: ImageRequest.Builder?
) : ActiveStreamData(data, artRequest)

class FetchedStreamData(
    data: StreamData,
    artRequest: ImageRequest.Builder?,
    val source: MediaSource
) : ActiveStreamData(data, artRequest)

class MediaPlayer(
    private val context: Context
) : AudioManager.OnAudioFocusChangeListener {
    private val scope = MainScope()
    private val handler = Handler(Looper.getMainLooper())
    private val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    private val focusRequest = AudioFocusRequestCompat.Builder(AudioManagerCompat.AUDIOFOCUS_GAIN)
        .setAudioAttributes(audioAttributes)
        .setOnAudioFocusChangeListener(this)
        .build()

    val queue = StreamQueue()
    val sources = DataSources(context, DefaultBandwidthMeter.Builder(context).build())
    var conn: PlayerConnection? = null
        private set
    private var fetchJob: Job? = null

    data class Position(val position: Long = 0, val buffered: Long = 0)

    private val _stream = MutableStateFlow<ActiveStreamData?>(null)
    val stream: StateFlow<ActiveStreamData?>
        get() = _stream

    private val _state = MutableStateFlow(STATE_FETCHING)
    val state: StateFlow<Int>
        get() = _state

    private val _duration = MutableStateFlow(DURATION_UNKNOWN)
    val duration: StateFlow<Long>
        get() = _duration

    private val _position = MutableStateFlow(Position())
    val position: StateFlow<Position>
        get() = _position

    init {
        scope.launch {
            queue.stream.collect { element ->
                if (element == null) {
                    _stream.value = null
                    release()
                } else {
                    _stream.value = FetchingStreamData(element, ImageRequest.Builder(context)
                        .data(element.artUrl))
                    _duration.value = DURATION_UNKNOWN
                    _state.value = STATE_FETCHING
                    val conn = requireConnection()
                    conn.exo.clearMediaItems()

                    fetchJob = scope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, ex ->
                        // todo error notif
                    }) {
                        val source = element.fetchSource(context, sources)
                        launch(Dispatchers.Main) {
                            _stream.value = source
                            conn.exo.setMediaSource(source.source)
                            conn.exo.prepare()
                        }
                    }
                }
            }
        }
    }

    private fun requestAudioFocus() = AudioManagerCompat.requestAudioFocus(audioManager, focusRequest)
    private fun abandonAudioFocus() = AudioManagerCompat.abandonAudioFocusRequest(audioManager, focusRequest)

    fun requireConnection(): PlayerConnection {
        return conn ?: PlayerConnection(
            context,
            { exoListener(it) },
            { queueNavigator(it) }
        ).also { conn ->
            this.conn = conn
            postUpdatePosition()
            ContextCompat.startForegroundService(context, Intent(context, PlayerService::class.java))
            Log.d(TAG, "MediaPlayer created")
        }
    }

    fun release() {
        val conn = conn ?: return
        fetchJob?.cancel()
        context.stopService(Intent(context, PlayerService::class.java))
        AudioManagerCompat.abandonAudioFocusRequest(audioManager, focusRequest)
        conn.release()
        this.conn = null
        queue.clear()
        Log.d(TAG, "MediaPlayer released")
    }

    private fun exoListener(conn: PlayerConnection) = object : Player.Listener {
        fun stateFromReady(playWhenReady: Boolean) =
            if (playWhenReady) STATE_PLAYING else STATE_PAUSED

        override fun onPlaybackStateChanged(playbackState: Int) {
            when (playbackState) {
                Player.STATE_IDLE, Player.STATE_BUFFERING -> _state.value = STATE_FETCHING
                Player.STATE_READY -> {
                    requestAudioFocus()
                    _state.value = stateFromReady(conn.exo.playWhenReady)
                    _duration.value = conn.exo.duration
                }
                Player.STATE_ENDED -> {
                    // if we've just cleared all media items
                    // (we're about to buffer the next item)
                    if (_state.value == STATE_FETCHING)
                        return
                    _state.value = STATE_PAUSED
                    skipNext()
                }
            }
        }

        override fun onPlayWhenReadyChanged(playWhenReady: Boolean, reason: Int) {
            if (playWhenReady) {
                requestAudioFocus()
                _state.value = STATE_PLAYING
                if (_stream.value?.data?.streamType?.live == true) {
                    seekToDefault()
                }
            } else {
                _state.value = STATE_PAUSED
            }
        }

        // NewPipe: player/Player.java
        override fun onPlayerError(ex: PlaybackException) {
            val exo = conn.exo
            when (ex.errorCode) {
                PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW -> {
                    exo.seekToDefaultPosition()
                    exo.prepare()
                }
                PlaybackException.ERROR_CODE_IO_INVALID_HTTP_CONTENT_TYPE,
                PlaybackException.ERROR_CODE_IO_BAD_HTTP_STATUS,
                PlaybackException.ERROR_CODE_IO_FILE_NOT_FOUND,
                PlaybackException.ERROR_CODE_IO_NO_PERMISSION,
                PlaybackException.ERROR_CODE_IO_CLEARTEXT_NOT_PERMITTED,
                PlaybackException.ERROR_CODE_IO_READ_POSITION_OUT_OF_RANGE,
                PlaybackException.ERROR_CODE_PARSING_CONTAINER_MALFORMED,
                PlaybackException.ERROR_CODE_PARSING_MANIFEST_MALFORMED,
                PlaybackException.ERROR_CODE_PARSING_CONTAINER_UNSUPPORTED,
                PlaybackException.ERROR_CODE_PARSING_MANIFEST_UNSUPPORTED -> {
                    Log.e(TAG, "Playback error", ex)
                    // TODO exception handler here
                    skipNext()
                }
                PlaybackException.ERROR_CODE_TIMEOUT,
                PlaybackException.ERROR_CODE_IO_UNSPECIFIED,
                PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_FAILED,
                PlaybackException.ERROR_CODE_IO_NETWORK_CONNECTION_TIMEOUT,
                PlaybackException.ERROR_CODE_UNSPECIFIED -> {
                    exo.prepare()
                }
                else -> release()
            }
        }
    }

    private fun queueNavigator(conn: PlayerConnection) = object : MediaSessionConnector.QueueNavigator {
        override fun getSupportedQueueNavigatorActions(player: Player) =
            PlaybackStateCompat.ACTION_SKIP_TO_NEXT or
                PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or
                PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM

        override fun onTimelineChanged(player: Player) {}
        override fun onCurrentMediaItemIndexChanged(player: Player) {}

        override fun getActiveQueueItemId(player: Player?) =
            queue.index.value.toLong()

        override fun onSkipToPrevious(player: Player) = skipPrevious()

        override fun onSkipToQueueItem(player: Player, id: Long) {
            queue.setIndex(id.toInt())
        }

        override fun onSkipToNext(player: Player) = skipNext()

        override fun onCommand(
            player: Player,
            command: String,
            extras: Bundle?,
            cb: ResultReceiver?
        ) = false
    }

    override fun onAudioFocusChange(focus: Int) {
        val conn = conn ?: return
        when (focus) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                animateVolume(VOLUME_DUCK, 1f, VOLUME_DUCK_TIME)
                conn.exo.play()
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                animateVolume(1f, VOLUME_DUCK, VOLUME_DUCK_TIME)
            }
            AudioManager.AUDIOFOCUS_LOSS, AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                conn.exo.pause()
            }
        }
    }

    private fun postUpdatePosition() {
        handler.postDelayed({
            conn?.let { conn ->
                updatePosition(conn)
                postUpdatePosition()
            }
        }, PROGRESS_UPDATE_PERIOD)
    }

    private fun updatePosition(conn: PlayerConnection) {
        _position.value = Position(conn.exo.currentPosition, conn.exo.bufferedPosition)
    }

    fun play() {
        val conn = conn ?: return
        if (_state.value == STATE_PLAYING) return
        if (conn.exo.playbackState == Player.STATE_ENDED || _stream.value?.data?.streamType?.live == true) {
            seekToDefault()
        }
        requestAudioFocus()
        _state.value = STATE_PLAYING
        conn.exo.play()
        animateVolume(0f, 1f, /*Prefs.volumeAnimTime(context)*/ 500)
    }

    fun pause() {
        val conn = conn ?: return
        if (_state.value == STATE_PAUSED) return
        abandonAudioFocus()
        // change state now, not at animation end
        // so visually (e.g. notifs) the state gets updated now
        _state.value = STATE_PAUSED
        animateVolume(1f, 0f, /*Prefs.volumeAnimTime(context)*/ 500) {
            conn.exo.pause()
        }
    }

    /*
    · explicit transport actions start playback
      (e.g. user presses skip next/previous button)
    · implicit queue changes do not start playback
      (e.g. item removed from queue)
     */
    fun skipNext() {
        if (queue.skipNext()) {
            play()
        }
    }

    fun skipPrevious() {
        if (queue.skipPrevious()) {
            play()
        }
    }

    fun restartOrSkipPrevious() {
        if (_position.value.position > RESTART_THRESHOLD) {
            seekToDefault()
        } else {
            skipPrevious()
        }
    }

    fun seekTo(position: Long) {
        val conn = conn ?: return
        conn.exo.seekTo(position)
        updatePosition(conn)
    }

    fun seekToDefault() {
        val conn = conn ?: return
        conn.exo.seekToDefaultPosition()
        updatePosition(conn)
    }

    fun animateVolume(from: Float, to: Float, duration: Long, onEnd: () -> Unit = {}) {
        ValueAnimator().apply {
            setFloatValues(from, to)
            this.duration = duration
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator?) {
                    conn?.exo?.volume = from
                }

                override fun onAnimationCancel(animation: Animator?) {
                    conn?.exo?.volume = to
                }

                override fun onAnimationEnd(animation: Animator?) {
                    conn?.exo?.volume = to
                    onEnd.invoke()
                }
            })
            addUpdateListener { conn?.exo?.volume = it.animatedValue as Float }
            start()
        }
    }

    companion object {
        val audioAttributes: AudioAttributesCompat = AudioAttributesCompat.Builder()
            .setContentType(AudioAttributesCompat.CONTENT_TYPE_MUSIC)
            .setUsage(AudioAttributesCompat.USAGE_MEDIA)
            .build()
    }
}

val StreamType.live: Boolean
    get() = this == StreamType.LIVE_STREAM || this == StreamType.AUDIO_LIVE_STREAM

val Context.player get() = (applicationContext as App).player
