package com.github.aecsocket.orphea.route

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.github.aecsocket.orphea.*
import com.github.aecsocket.orphea.R
import com.github.aecsocket.orphea.media.*
import kotlinx.coroutines.*
import org.schabi.newpipe.extractor.stream.StreamType
import java.io.IOException

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SearchServices(
    selected: Int,
    onValueChange: (Int) -> Unit = {}
) {
    LazyRow(
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(horizontal = 8.dp),
        modifier = Modifier.fillMaxWidth(),
    ) {
        items(DataServices.All.size) { idx ->
            val service = DataServices.All[idx]
            Chip(
                onClick = { onValueChange(idx) },
                colors = if (selected == idx) ChipDefaults.chipColorsSelected() else ChipDefaults.chipColors(),
            ) { Text(text = service.name()) }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchHeader(
    query: String,
    service: Int,
    modifier: Modifier = Modifier,
    onQueryChange: (String) -> Unit = {},
    onServiceChange: (Int) -> Unit = {},
    onSearch: () -> Unit = {},
) {
    val focusManager = LocalFocusManager.current
    Column(modifier = modifier) {
        fun submit() {
            focusManager.clearFocus()
            onSearch()
        }

        TextField(
            value = query,
            onValueChange = { onQueryChange(it.split('\n')[0]) },
            label = { Text(stringResource(R.string.search)) },
            singleLine = true,
            maxLines = 1,
            keyboardOptions = KeyboardOptions(
                autoCorrect = false,
                keyboardType = KeyboardType.Uri,
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions { submit() },
            trailingIcon = {
                if (query.isNotEmpty()) {
                    IconButton(onClick = { onQueryChange("") }) {
                        IconImage(
                            painter = painterResource(R.drawable.ic_clear),
                            contentDescription = stringResource(R.string.clear_search)
                        )
                    }
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
                .onKeyEvent { event ->
                    return@onKeyEvent if (event.key == Key.Enter) {
                        submit()
                        true
                    } else false
                },
        )

        SearchServices(
            selected = service,
            onValueChange = onServiceChange,
        )
    }
}

@Composable
fun SearchFetching() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize(),
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun SearchNoResults() {
    Column(modifier = Modifier.padding(32.dp)) {
        Text(
            text = stringResource(R.string.no_results),
            style = MaterialTheme.typography.h4
        )
        Text(
            text = stringResource(R.string.no_results_sub)
        )
    }
}

@Composable
fun SearchNoItems() {
    Column(modifier = Modifier.padding(32.dp)) {
        Text(
            text = stringResource(R.string.no_results),
            style = MaterialTheme.typography.h4
        )
    }
}

@Composable
fun SearchItemComponent(
    data: ItemData,
    added: Boolean,
    onClick: () -> Unit,
    onAdd: () -> Unit,
    onRemove: () -> Unit,
) {
    Row(modifier = Modifier.clickable(onClick = onClick)) {
        ItemComponent(data = data, modifier = Modifier
            .weight(1f))

        when (data) {
            is StreamData, is StreamListData -> {
                IconButton(
                    onClick = if (added) onRemove else onAdd,
                    modifier = Modifier
                        .padding(horizontal = 4.dp)
                        .align(Alignment.CenterVertically)
                ) {
                    IconImage(
                        painter = painterResource(if (added) R.drawable.ic_check else R.drawable.ic_enqueue),
                        contentDescription = stringResource(if (added) R.string.remove_from_queue else R.string.add_to_queue)
                    )
                }
            }
            else -> {}
        }
    }
}

@Composable
fun SearchItems(
    items: List<ItemData>,
    added: (ItemData) -> Boolean,
    onClickItem: (ItemData) -> Unit,
    onAddItem: (ItemData) -> Unit,
    onRemoveItem: (ItemData) -> Unit,
) {
    LazyColumn {
        item {
            Spacer(modifier = Modifier.padding(4.dp))
        }

        items(items.size) {
            val data = items[it]
            SearchItemComponent(
                data = data,
                added = added(data),
                onClick = { onClickItem(data) },
                onAdd = { onAddItem(data) },
                onRemove = { onRemoveItem(data) },
            )
        }
    }
}

@Composable
fun SearchCategory(
    category: ItemCategory,
    added: (ItemData) -> Boolean,
    onClickItem: (ItemData) -> Unit,
    onAddItem: (ItemData) -> Unit,
    onRemoveItem: (ItemData) -> Unit,
    onViewAll: () -> Unit,
) {
    val items = category.items.take(3)
    Column {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onViewAll)
                .padding(start = 16.dp)
        ) {
            Text(
                text = category.name(),
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .weight(1f),
            )

            IconImage(
                painter = painterResource(R.drawable.ic_forward),
                contentDescription = stringResource(R.string.see_all),
                modifier = Modifier
                    .padding(16.dp),
            )
        }

        items.forEach {
            SearchItemComponent(
                data = it,
                added = added(it),
                onClick = { onClickItem(it) },
                onAdd = { onAddItem(it) },
                onRemove = { onRemoveItem(it) },
            )
        }
    }
}

@Composable
fun SearchCategories(
    results: List<ItemCategory> = emptyList(),
    added: (ItemData) -> Boolean,
    onClickItem: (ItemData) -> Unit,
    onAddItem: (ItemData) -> Unit,
    onRemoveItem: (ItemData) -> Unit,
    onViewCategory: (ItemCategory) -> Unit
) {
    LazyColumn {
        items(results.size) { idx ->
            val category = results[idx]
            SearchCategory(
                category = category,
                added = added,
                onClickItem = onClickItem,
                onAddItem = onAddItem,
                onRemoveItem = onRemoveItem,
                onViewAll = { onViewCategory(category) }
            )
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchErrors(
    errors: List<Throwable> = emptyList()
) {
    val context = LocalContext.current
    val errorInfo = errors.errorInfo()

    Column(modifier = Modifier.padding(32.dp)) {
        Text(
            text = pluralStringResource(
                R.plurals.errors_occurred, errors.size,
                errors.size),
            style = MaterialTheme.typography.h4,
        )

        Spacer(Modifier.height(8.dp))

        LazyColumn {
            items(errors.size) { idx ->
                val error = errors[idx]
                Text(text = errorMessage(error))
            }
        }

        Spacer(Modifier.height(8.dp))

        Button(
            onClick = { context.startActivity(errorActivityIntent(context, errorInfo)) }
        ) {
            Text(text = stringResource(R.string.details))
        }
    }
}

data class SearchPageState(
    val viewModel: SearchViewModel = SearchViewModel(),
    val service: Int = DataServices.All.indexOfFirst { it.couldBeDefault },
    val query: String = ""
)

@Composable
fun SearchPage(
    onClickItem: (ItemData) -> Unit,
    added: (ItemData) -> Boolean,
    onAddItem: (ItemData) -> Unit,
    onRemoveItem: (ItemData) -> Unit,
) {
    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    val scaffold = rememberScaffoldState()
    val navController = rememberNavController()
    var state by remember { mutableStateOf(SearchPageState()) }
    val viewModel = state.viewModel

    if (viewModel.suggestions.errors.isNotEmpty()) {
        ErrorSnackbar(
            scaffold = scaffold,
            message = R.string.error_fetch_suggestions,
            error = viewModel.suggestions.errors.errorInfo()
        )
        // clear errors
        viewModel.clearSuggestions()
    }

    fun fetchResults() {
        viewModel.fetchSearch(context, scope, state.service, state.query)
    }

    fun fetchSuggestions() {
        viewModel.fetchSuggestions(context, scope, state.service, state.query)
    }

    Scaffold(
        modifier = Modifier,
        scaffoldState = scaffold
    ) { padding ->
        Column(modifier = Modifier.padding(padding)) {
            Surface(
                elevation = 4.dp
            ) {
                SearchHeader(
                    service = state.service,
                    query = state.query,
                    onQueryChange = {
                        state = state.copy(query = it)
                        fetchSuggestions()
                    },
                    onServiceChange = {
                        state = state.copy(service = it)
                    },
                    onSearch = {
                        // todo pop up to X
                        navController.navigate("results")
                        fetchResults()
                    }
                )
            }

            NavHost(navController = navController, startDestination = "results") {
                composable("results") {
                    when (val resultsState = viewModel.search) {
                        is SearchState.Empty -> {}
                        is SearchState.Fetching -> {
                            SearchFetching()
                        }
                        is SearchState.Errors -> {
                            SearchErrors(resultsState.errors)
                        }
                        is SearchState.Items -> {
                            if (resultsState.items.isEmpty()) {
                                SearchNoResults()
                            } else {
                                SearchItems(
                                    items = resultsState.items,
                                    added = added,
                                    onClickItem = onClickItem,
                                    onAddItem = onAddItem,
                                    onRemoveItem = onRemoveItem,
                                )
                            }
                        }
                        is SearchState.Categories -> {
                            if (resultsState.items.isEmpty()) {
                                SearchNoResults()
                            } else {
                                SearchCategories(
                                    results = resultsState.items,
                                    added = added,
                                    onClickItem = onClickItem,
                                    onAddItem = onAddItem,
                                    onRemoveItem = onRemoveItem,
                                    onViewCategory = {
                                        viewModel.fetchItems(context, scope, it.type)
                                        navController.navigate("category")
                                    }
                                )
                                if (resultsState.errors.isNotEmpty()) {
                                    ErrorSnackbar(
                                        scaffold = scaffold,
                                        message = R.string.error_fetch_search,
                                        error = resultsState.errors.errorInfo()
                                    )
                                }
                            }
                        }
                    }

                    val suggestions = viewModel.suggestions
                    if (suggestions.errors.isNotEmpty()) {
                        ErrorSnackbar(
                            scaffold = scaffold,
                            message = R.string.error_fetch_suggestions,
                            error = suggestions.errors.errorInfo()
                        )
                    }
                }

                composable("category") {
                    when (val itemsState = viewModel.items) {
                        is ItemsState.Empty -> {}
                        is ItemsState.Fetching -> {
                            SearchFetching()
                        }
                        is ItemsState.Errors -> {
                            SearchErrors(itemsState.errors)
                        }
                        is ItemsState.Items -> {
                            if (itemsState.items.isEmpty()) {
                                SearchNoItems()
                            } else {
                                SearchItems(
                                    items = itemsState.items,
                                    added = added,
                                    onClickItem = onClickItem,
                                    onAddItem = onAddItem,
                                    onRemoveItem = onRemoveItem,
                                )
                                if (itemsState.errors.isNotEmpty()) {
                                    ErrorSnackbar(
                                        scaffold = scaffold,
                                        message = R.string.error_fetch_search,
                                        error = itemsState.errors.errorInfo()
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewSearchHeader() {
    OrpheaPreview {
        SearchHeader("", 0)
    }
}

@Preview
@Composable
fun PreviewSearchFetching() {
    OrpheaPreview {
        SearchFetching()
    }
}

@Preview
@Composable
fun PreviewSearchResults() {
    OrpheaPreview {
        SearchCategories(
            results = listOf(
                ItemCategory(
                    CATEGORY_SONGS, listOf(
                    StreamData("..", DummyDataService, "Song 1", "MF DOOM", "..", STREAM_SONG, 1000 * 60 * 4, StreamType.VIDEO_STREAM),
                    StreamData("..", DummyDataService, "Song 2", "Blur", "..", STREAM_SONG, 1000 * 60 * 3, StreamType.VIDEO_STREAM),
                )), ItemCategory(
                    CATEGORY_ARTISTS, listOf(
                    UploaderData("..", DummyDataService, "Artist 1", "..", UPLOADER_ARTIST),
                    UploaderData("..", DummyDataService, "Artist 2", "..", UPLOADER_ARTIST),
                ))
            ),
            added = { false },
            onClickItem = {},
            onAddItem = {},
            onRemoveItem = {},
            onViewCategory = {}
        )
    }
}

@Preview
@Composable
fun PreviewSearchNoResults() {
    OrpheaPreview {
        SearchNoResults()
    }
}

@Preview
@Composable
fun PreviewSearchErrors() {
    OrpheaPreview {
        SearchErrors(errors = listOf(
            InvalidServiceException,
            IOException("Lookup failed")
        ))
    }
}

object InvalidServiceException : RuntimeException()

sealed interface SearchState {
    object Empty : SearchState
    object Fetching : SearchState
    data class Items(
        val items: List<ItemData>
    ) : SearchState
    data class Categories(
        val items: List<ItemCategory>,
        val errors: List<Throwable>
    ) : SearchState
    data class Errors(
        val errors: List<Throwable>
    ) : SearchState
}

sealed interface ItemsState {
    object Empty : ItemsState
    object Fetching : ItemsState
    data class Items(
        val items: List<ItemData>,
        val errors: List<Throwable>
    ) : ItemsState
    data class Errors(
        val errors: List<Throwable>
    ) : ItemsState
}

class SearchViewModel : ViewModel() {
    var search: SearchState by mutableStateOf(SearchState.Empty)
        private set
    var items: ItemsState by mutableStateOf(ItemsState.Empty)
        private set
    var suggestions: Errorable<List<String>> by mutableStateOf(Errorable(emptyList()))
        private set

    private data class Query(
        val query: String,
        val service: DataService,
        val results: MutableMap<Int, List<ItemData>>
    )

    private var thisQuery: Query? = null

    private fun service(index: Int): DataService? {
        return if (index < 0 || index >= DataServices.All.size) null
            else DataServices.All[index]
    }

    fun fetchSearch(
        context: Context,
        scope: CoroutineScope,
        serviceIndex: Int,
        query: String
    ) {
        thisQuery = null
        search = SearchState.Fetching

        if (!hasNetwork(context)) {
            search = SearchState.Errors(listOf(NoNetworkException))
            return
        }

        scope.launch {
            DataServices.byUrl(query)?.let { service ->
                scope.launch(Dispatchers.IO) {
                    search = service.fetchUrl(query)?.let {
                        SearchState.Items(listOf(it))
                    } ?: SearchState.Empty
                }
            } ?: run {
                service(serviceIndex)?.let { service ->
                    scope.launch(Dispatchers.IO) {
                        val (results, errors) = service.fetchSearch(query)
                        thisQuery = Query(
                            query, service,
                            results
                                .map { it.type to it.items }
                                .associate { it }
                                .toMutableMap()
                        )
                        search = SearchState.Categories(results, errors)
                    }
                } ?: run { search = SearchState.Errors(listOf(InvalidServiceException)) }
            }
        }
    }

    fun fetchItems(
        context: Context,
        scope: CoroutineScope,
        category: Int
    ) {
        val query = thisQuery ?: return
        items = ItemsState.Fetching

        if (!hasNetwork(context)) {
            items = ItemsState.Errors(listOf(NoNetworkException))
            return
        }

        scope.launch(Dispatchers.IO) {
            val (results, errors) = query.service.fetchSearch(query.query, category)
            items = ItemsState.Items(results, errors)
        }
    }

    fun fetchSuggestions(
        context: Context,
        scope: CoroutineScope,
        serviceIndex: Int,
        query: String
    ) {
        if (query.isEmpty()) {
            suggestions = Errorable(emptyList())
            return
        }

        if (!hasNetwork(context))
            return

        service(serviceIndex)?.let { service ->
            scope.launch(Dispatchers.IO) {
                suggestions = service.fetchSuggestions(query)
            }
        }
    }

    fun clearSuggestions() {
        suggestions = Errorable(emptyList())
    }
}


/*if (suggestions.result.isNotEmpty()) {
    DropdownMenu(
        expanded = state.expandedSuggestions,
        onDismissRequest = { state = state.copy(expandedSuggestions = false) },
        modifier = Modifier.fillMaxWidth(),
    ) {
        suggestions.result.forEach {
            DropdownMenuItem(onClick = {
                state = state.copy(query = it)
                fetchResults()
            }) {
                Text(
                    text = it,
                    modifier = Modifier.weight(1f),
                )
                IconButton(onClick = {
                    state = state.copy(query = it)
                    fetchSuggestions()
                }) {
                    IconImage(
                        painter = painterResource(R.drawable.ic_outward),
                        contentDescription = stringResource(R.string.to_search_field)
                    )
                }
            }
        }
    }*/
/*Surface(modifier = Modifier.fillMaxSize()) {
    SearchSuggestions(
        suggestions = suggestions.result,
        onClick = {
            state = state.copy(query = it)
            fetchResults()
        },
        onToField = {
            state = state.copy(query = it)
            fetchSuggestions()
        }
    )
}
}*/
