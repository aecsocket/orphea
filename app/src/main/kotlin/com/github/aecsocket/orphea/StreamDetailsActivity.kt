package com.github.aecsocket.orphea

import android.content.res.Configuration
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.imageLoader
import com.github.aecsocket.orphea.player.DURATION_UNKNOWN
import com.github.aecsocket.orphea.player.STATE_FETCHING
import com.github.aecsocket.orphea.player.STATE_PLAYING
import com.github.aecsocket.orphea.player.player
import com.github.aecsocket.orphea.ui.theme.OrpheaTheme

class StreamDetailsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            OrpheaTheme {
                Surface(modifier = Modifier.fillMaxSize()) {
                    PlayerSteamDetails()
                }
            }
        }
    }
}

@Composable
fun StreamDetails(
    name: String,
    uploader: String,
    artUrl: String,
    position: Long,
    duration: Long?,
    playing: Boolean,
    onSeek: (Float) -> Unit,
    onPlay: () -> Unit,
    onPause: () -> Unit,
    onSkipNext: () -> Unit,
    onSkipPrevious: () -> Unit,
) {
    Column(modifier = Modifier.fillMaxSize()) {
        Box(modifier = Modifier
            .weight(1f)
            .padding(vertical = 24.dp)
        ) {
            AsyncImage(
                model = artUrl,
                contentDescription = name,
                alignment = Alignment.Center,
                modifier = Modifier.fillMaxSize(),
            )
        }

        Column(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 24.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 24.dp)
            ) {
                Marquee(
                    duration = 10_000,
                    repeatAfter = 3_000,
                ) {
                    Text(
                        text = name,
                        style = MaterialTheme.typography.h4,
                        fontWeight = FontWeight.Bold,
                        maxLines = 1,
                    )
                }
                Text(
                    text = uploader,
                    style = MaterialTheme.typography.h5,
                    maxLines = 1,
                )
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                @Composable
                fun Time(ms: Long?, align: TextAlign) {
                    Text(
                        text = formatOptTime(ms),
                        maxLines = 1,
                        overflow = TextOverflow.Clip,
                        textAlign = align,
                        modifier = Modifier
                            .padding(horizontal = 6.dp)
                            .width(64.dp)
                            .align(Alignment.CenterVertically),
                    )
                }

                Time(ms = position, align = TextAlign.Right)

                Slider(
                    value = duration?.let { position.toFloat() / it } ?: 0f,
                    onValueChange = onSeek,
                    modifier = Modifier.weight(1f)
                )

                Time(ms = duration, align = TextAlign.Left)
            }

            Row(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
            ) {
                @Composable
                fun TransportButton(
                    onClick: () -> Unit,
                    @DrawableRes imageRes: Int,
                    @StringRes descriptionRes: Int,
                    modifier: Modifier,
                    imageModifier: Modifier,
                ) {
                    IconButton(
                        onClick = onClick,
                        modifier = modifier
                            .align(Alignment.CenterVertically)
                    ) {
                        IconImage(
                            painter = painterResource(imageRes),
                            contentDescription = stringResource(descriptionRes),
                            modifier = imageModifier,
                        )
                    }
                }

                @Composable
                fun Large(
                    onClick: () -> Unit,
                    @DrawableRes imageRes: Int,
                    @StringRes descriptionRes: Int,
                ) {
                    TransportButton(
                        onClick = onClick,
                        imageRes = imageRes,
                        descriptionRes = descriptionRes,
                        modifier = Modifier.size(96.dp),
                        imageModifier = Modifier.size(64.dp),
                    )
                }

                @Composable
                fun Small(
                    onClick: () -> Unit,
                    @DrawableRes imageRes: Int,
                    @StringRes descriptionRes: Int,
                ) {
                    TransportButton(
                        onClick = onClick,
                        imageRes = imageRes,
                        descriptionRes = descriptionRes,
                        modifier = Modifier.size(64.dp),
                        imageModifier = Modifier.size(48.dp),
                    )
                }

                Small(
                    onClick = onSkipPrevious,
                    imageRes = R.drawable.ic_skip_previous,
                    descriptionRes = R.string.skip_previous
                )

                Large(
                    onClick = if (playing) onPause else onPlay,
                    imageRes = if (playing) R.drawable.ic_pause else R.drawable.ic_play,
                    descriptionRes = if (playing) R.string.pause else R.string.play,
                )

                Small(
                    onClick = onSkipNext,
                    imageRes = R.drawable.ic_skip_next,
                    descriptionRes = R.string.skip_next,
                )
            }
        }
    }
}

@Composable
fun PlayerSteamDetails() {
    val context = LocalContext.current
    val player = context.player
    val state by player.state.collectAsState()
    val position by player.position.collectAsState()
    val duration by player.duration.collectAsState()
    player.stream.collectAsState().value?.let { stream ->
        StreamDetails(
            name = stream.data.name,
            uploader = stream.data.uploader,
            artUrl = stream.data.artUrl,
            position = position.position,
            duration = if (duration == DURATION_UNKNOWN) null else duration,
            playing = when (state) {
                STATE_PLAYING, STATE_FETCHING -> true
                else -> false
            },
            onSeek = {
                if (duration != DURATION_UNKNOWN) {
                    player.seekTo((it * duration).toLong())
                }
            },
            onPlay = { player.play() },
            onPause = { player.pause() },
            onSkipNext = { player.skipNext() },
            onSkipPrevious = { player.restartOrSkipPrevious() },
        )
    }
}

@Preview("Light Mode")
@Preview("Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewStreamDetails() {
    OrpheaPreview(Modifier.fillMaxSize()) {
        StreamDetails(
            name = "One Two Three Four Five",
            uploader = "MF DOOM",
            artUrl = "https://upload.wikimedia.org/wikipedia/en/8/8d/MGMT_-_Little_Dark_Age.png",
            position = 30_000,
            duration = 60_000,
            playing = false,
            onSeek = {},
            onPlay = {},
            onPause = {},
            onSkipNext = {},
            onSkipPrevious = {},
        )
    }
}
