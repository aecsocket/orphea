package com.github.aecsocket.orphea

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily.Companion.Monospace
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.github.aecsocket.orphea.route.InvalidServiceException
import com.github.aecsocket.orphea.ui.theme.OrpheaTheme
import java.io.IOException

const val ERROR_INFO = "error_info"

class ErrorActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent.getParcelableExtra<ErrorInfo>(ERROR_INFO)?.let {
            setContent {
                OrpheaTheme {
                    Surface(modifier = Modifier.fillMaxSize()) {
                        ErrorComponent(it)
                    }
                }
            }
        } ?: finish()
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ErrorComponent(error: ErrorInfo) {
    LazyColumn {
        item {
            Text(
                text = pluralStringResource(R.plurals.errors_occurred, error.parts.size,
                    error.parts.size),
                style = MaterialTheme.typography.h4,
                modifier = Modifier
                    .padding(16.dp)
            )
        }

        items(error.parts.size) { idx ->
            val part = error.parts[idx]
            ErrorPartComponent(part = part)
        }
    }
}

@Composable
fun ErrorPartComponent(part: ErrorPart) {
    var expanded by remember { mutableStateOf(false) }
    Row(modifier = Modifier
        .fillMaxWidth()
        .clickable { expanded = !expanded }
    ) {
        Column {
            Row {
                Text(
                    text = part.message,
                    style = MaterialTheme.typography.body1,
                    modifier = Modifier
                        .weight(1f)
                        .padding(horizontal = 16.dp, vertical = 8.dp),
                )
                
                IconImage(
                    painter = painterResource(if (expanded) R.drawable.ic_expand_less
                        else R.drawable.ic_expand_more),
                    contentDescription = stringResource(R.string.see_more),
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(horizontal = 16.dp)
                )
            }
            
            if (expanded) {
                LazyRow(
                    contentPadding = PaddingValues(horizontal = 16.dp),
                    modifier = Modifier
                        .animateContentSize()
                        .padding(vertical = 8.dp)
                ) {
                    item {
                        Text(
                            text = part.detail,
                            style = MaterialTheme.typography.body2,
                            fontFamily = Monospace,
                        )
                    }
                }
            }
        }
    }
}

@Preview("Light Mode")
@Preview("Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewError() {
    OrpheaPreview(Modifier.fillMaxSize()) {
        ErrorComponent(listOf(
            IOException("Network error"),
            InvalidServiceException
        ).errorInfo())
    }
}
