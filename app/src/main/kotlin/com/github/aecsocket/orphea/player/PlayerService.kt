package com.github.aecsocket.orphea.player

import android.app.Notification
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Binder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import coil.imageLoader
import coil.request.Disposable
import com.github.aecsocket.orphea.*
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

const val ACTION_PLAY = "$PKG.player.PlayerService.PLAY"
const val ACTION_PAUSE = "$PKG.player.PlayerService.PAUSE"
const val ACTION_SKIP_NEXT = "$PKG.player.PlayerService.SKIP_NEXT"
const val ACTION_SKIP_PREVIOUS = "$PKG.player.PlayerService.SKIP_PREVIOUS"
const val ACTION_STOP = "$PKG.player.PlayerService.STOP"
const val NOTIFICATION_ID = 84928

class PlayerService : LifecycleService() {
    private val binder = PlayerBinder()
    private lateinit var player: MediaPlayer
    private lateinit var receiver: BroadcastReceiver
    private var art: Bitmap? = null
    private var queuedArt: Disposable? = null

    private fun ActiveStreamData?.assert() = this
        ?: throw IllegalStateException("Using PlayerService with no stream")

    private val stream: ActiveStreamData
        get() = player.stream.value.assert()

    override fun onCreate() {
        super.onCreate()
        player = (application as App).player

        fun <V> collect(state: StateFlow<V>, collector: FlowCollector<V>) {
            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    state.collect(collector)
                }
            }
        }

        collect(player.state) { updateNotification(stream) }
        collect(player.stream) { it?.let { updateStream(it) } }

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    ACTION_PLAY -> player.play()
                    ACTION_PAUSE -> player.pause()
                    ACTION_SKIP_NEXT -> player.skipNext()
                    ACTION_SKIP_PREVIOUS -> player.restartOrSkipPrevious()
                    ACTION_STOP -> player.release()
                    else -> return
                }

                if (player.conn != null) {
                    updateNotification(stream)
                }
            }
        }
        registerReceiver(receiver, IntentFilter().apply {
            addAction(Intent.ACTION_MEDIA_BUTTON)
            addAction(ACTION_PLAY)
            addAction(ACTION_PAUSE)
            addAction(ACTION_SKIP_NEXT)
            addAction(ACTION_SKIP_PREVIOUS)
            addAction(ACTION_STOP)
        })

        startForeground(NOTIFICATION_ID, createNotification(stream))
        Log.d(TAG, "PlayerService created")
    }

    override fun onBind(intent: Intent): PlayerBinder {
        super.onBind(intent)
        return binder
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
        player.release()
        Log.d(TAG, "PlayerService destroyed")
    }

    private fun updateNotification(stream: ActiveStreamData) {
        NotificationManagerCompat.from(this)
            .notify(NOTIFICATION_ID, createNotification(stream))
    }

    private fun createNotification(stream: ActiveStreamData): Notification {
        val conn = player.conn ?: throw IllegalStateException("Using PlayerService with no connection")
        val data = stream.data
        val flags = PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        val context = this
        return NotificationCompat.Builder(this, CHANNEL_MEDIA).apply {
            priority = NotificationCompat.PRIORITY_HIGH
            setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            setCategory(NotificationCompat.CATEGORY_TRANSPORT)
            setSmallIcon(R.drawable.ic_library) // todo
            setShowWhen(false)
            setContentTitle(data.name)
            setTicker(data.name)
            setContentText(data.uploader)
            setLargeIcon(art) // todo
            setContentIntent(PendingIntent.getActivity(
                context, 0, Intent(context, MainActivity::class.java), flags))

            addAction(R.drawable.ic_skip_previous, getString(R.string.skip_previous),
                PendingIntent.getBroadcast(context, 0, Intent(ACTION_SKIP_PREVIOUS), flags))

            when (player.state.value) {
                STATE_FETCHING -> addAction(R.drawable.ic_fetching, getString(R.string.fetching), null)
                STATE_PAUSED -> addAction(R.drawable.ic_play, getString(R.string.play),
                    PendingIntent.getBroadcast(context, 0, Intent(ACTION_PLAY), flags))
                STATE_PLAYING -> addAction(R.drawable.ic_pause, getString(R.string.pause),
                    PendingIntent.getBroadcast(context, 0, Intent(ACTION_PAUSE), flags))
            }

            addAction(R.drawable.ic_skip_next, getString(R.string.skip_next),
                PendingIntent.getBroadcast(context, 0, Intent(ACTION_SKIP_NEXT), flags))

            setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                .setMediaSession(conn.session.sessionToken)
                .setShowActionsInCompactView(1, 2))
        }.build()
    }

    private fun updateStream(stream: ActiveStreamData) {
        queuedArt?.dispose()
        stream.artRequest?.let { artRequest ->
            queuedArt = applicationContext.imageLoader.enqueue(artRequest
                .target {
                    art = (it as BitmapDrawable).bitmap
                    updateNotification(stream)
                }
                .build()
            )
        }
        updateNotification(stream)
    }

    inner class PlayerBinder : Binder() {
        val service: PlayerService
            get() = this@PlayerService
    }
}
