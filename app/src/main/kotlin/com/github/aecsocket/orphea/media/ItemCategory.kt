package com.github.aecsocket.orphea.media

import android.os.Parcelable
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.github.aecsocket.orphea.R
import kotlinx.parcelize.Parcelize

const val CATEGORY_SONGS = 0
const val CATEGORY_VIDEOS = 1
const val CATEGORY_ARTISTS = 2
const val CATEGORY_CHANNELS = 3
const val CATEGORY_ALBUMS = 4
const val CATEGORY_PLAYLISTS = 5

data class ItemCategory(
    val type: Int,
    val items: List<ItemData>
) {
    @Composable
    fun name() = stringResource(when (type) {
        CATEGORY_SONGS -> R.string.songs
        CATEGORY_VIDEOS -> R.string.videos
        CATEGORY_ARTISTS -> R.string.artists
        CATEGORY_CHANNELS -> R.string.channels
        CATEGORY_ALBUMS -> R.string.albums
        CATEGORY_PLAYLISTS -> R.string.playlists
        else -> throw IllegalArgumentException("unknown category type $type")
    })
}

fun StreamData.category() = when (type) {
    STREAM_SONG -> CATEGORY_SONGS
    STREAM_VIDEO -> CATEGORY_VIDEOS
    else -> throw IllegalArgumentException("unknown stream type $type")
}

fun UploaderData.category() = when (type) {
    UPLOADER_ARTIST -> CATEGORY_ARTISTS
    UPLOADER_CHANNEL -> CATEGORY_CHANNELS
    else -> throw IllegalArgumentException("unknown uploader type $type")
}

fun StreamListData.category() = when (type) {
    STREAM_LIST_ALBUM -> CATEGORY_ALBUMS
    STREAM_LIST_PLAYLIST -> CATEGORY_PLAYLISTS
    else -> throw IllegalArgumentException("unknown stream list type $type")
}

fun ItemData.category() = when (this) {
    is StreamData -> category()
    is UploaderData -> category()
    is StreamListData -> category()
}

fun ItemData.asCategory() = ItemCategory(category(), listOf(this))
