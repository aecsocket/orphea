package com.github.aecsocket.orphea.media

import android.content.Context
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import coil.request.ImageRequest
import com.github.aecsocket.orphea.Errorable
import com.github.aecsocket.orphea.TAG
import com.github.aecsocket.orphea.errorable
import com.github.aecsocket.orphea.player.DataSources
import com.github.aecsocket.orphea.player.FetchedStreamData
import com.github.aecsocket.orphea.player.live
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.MediaSource
import kotlinx.coroutines.*
import org.schabi.newpipe.extractor.InfoItem
import org.schabi.newpipe.extractor.MediaFormat
import org.schabi.newpipe.extractor.StreamingService
import org.schabi.newpipe.extractor.channel.ChannelInfo
import org.schabi.newpipe.extractor.channel.ChannelInfoItem
import org.schabi.newpipe.extractor.playlist.PlaylistInfo
import org.schabi.newpipe.extractor.playlist.PlaylistInfoItem
import org.schabi.newpipe.extractor.search.SearchInfo
import org.schabi.newpipe.extractor.stream.AudioStream
import org.schabi.newpipe.extractor.stream.StreamInfo
import org.schabi.newpipe.extractor.stream.StreamInfoItem

const val STREAM_QUALITY_QUALITY = "quality"
const val STREAM_QUALITY_EFFICIENCY = "efficiency"

private val YT_SONG_SUFFIXES = listOf(
    " - Topic",
    " — Topic"
)

data class DataItemTypes(
    val stream: Int,
    val uploader: Int,
    val streamList: Int
) {
    companion object {
        val Songs = DataItemTypes(STREAM_SONG, UPLOADER_ARTIST, STREAM_LIST_ALBUM)
        val Videos = DataItemTypes(STREAM_VIDEO, UPLOADER_CHANNEL, STREAM_LIST_PLAYLIST)
    }
}

class NewPipeDataService(
    val service: StreamingService,
    val itemTypes: DataItemTypes,
    val filters: Map<Int, List<String>>,
    val nameRes: Int? = null
) : DataService {
    @Composable
    override fun name(): String =
        nameRes?.let { stringResource(it) } ?: service.serviceInfo.name
    override val couldBeDefault
        get() = true

    override suspend fun fetchUrl(url: String): ItemData? {
        println(" > link type = ${service.getLinkTypeByUrl(url)} | srv = $service")
        return when (service.getLinkTypeByUrl(url)) {
            StreamingService.LinkType.STREAM -> {
                println("oh my god its a stream !!!!!!! ")
                StreamInfo.getInfo(service, url).asData()
            }
            StreamingService.LinkType.CHANNEL -> ChannelInfo.getInfo(service, url).asData()
            StreamingService.LinkType.PLAYLIST -> PlaylistInfo.getInfo(service, url).asData()
            else -> null
        }
    }

    private fun search(query: String, filters: List<String>) = SearchInfo.getInfo(service,
        service.searchQHFactory.fromQuery(query, filters, ""))

    private fun convertItem(item: InfoItem) = when(item) {
        is StreamInfoItem -> item.asData()
        is ChannelInfoItem -> item.asData()
        is PlaylistInfoItem -> item.asData()
        else -> null
    }

    private fun StreamInfoItem.asData(): StreamData {
        val defType = itemTypes.streamList
        val duration = duration * 1000
        return asSongArtist(uploaderName)?.let {
            StreamData(url, this@NewPipeDataService, name, it, thumbnailUrl, STREAM_SONG, duration, streamType)
        } ?: StreamData(url, this@NewPipeDataService, name, uploaderName, thumbnailUrl, defType, duration, streamType)
    }

    private fun StreamInfo.asData(): StreamData {
        val defType = itemTypes.stream
        val duration = duration * 1000
        return asSongArtist(uploaderName)?.let {
            StreamData(url, this@NewPipeDataService, name, it, thumbnailUrl, STREAM_SONG, duration, streamType)
        } ?: StreamData(url, this@NewPipeDataService, name, uploaderName, thumbnailUrl, defType, duration, streamType)
    }

    private fun ChannelInfoItem.asData(): UploaderData {
        val defType = itemTypes.uploader
        return asSongArtist(name)?.let {
            UploaderData(url, this@NewPipeDataService, it, thumbnailUrl, UPLOADER_ARTIST)
        } ?: UploaderData(url, this@NewPipeDataService, name, thumbnailUrl, defType)
    }

    private fun ChannelInfo.asData(): UploaderData {
        val defType = itemTypes.uploader
        return asSongArtist(name)?.let {
            UploaderData(url, this@NewPipeDataService, it, avatarUrl, UPLOADER_ARTIST)
        } ?: UploaderData(url, this@NewPipeDataService, name, avatarUrl, defType)
    }

    private fun PlaylistInfoItem.asData(): StreamListData {
        val defType = itemTypes.streamList
        val size = if (streamCount < 0) null else streamCount
        return asSongArtist(uploaderName)?.let {
            StreamListData(url, this@NewPipeDataService, name, it, thumbnailUrl, defType, size)
        } ?: StreamListData(url, this@NewPipeDataService, name, uploaderName, thumbnailUrl, defType, size)
    }

    private fun PlaylistInfo.asData(): StreamListData {
        val defType = itemTypes.streamList
        val size = if (streamCount < 0) null else streamCount
        return asSongArtist(uploaderName)?.let {
            StreamListData(url, this@NewPipeDataService, name, it, thumbnailUrl, defType, size)
        } ?: StreamListData(url, this@NewPipeDataService, name, uploaderName, thumbnailUrl, defType, size)
    }

    override suspend fun fetchSearch(query: String) = errorable {
        val res = HashMap<Int, MutableMap<String, ItemData>>()
        filters.forEach { (_, forCategory) ->
            catching {
                val search = search(query, forCategory)
                search.relatedItems.forEach {
                    convertItem(it)?.let { data ->
                        res.computeIfAbsent(data.category()) { LinkedHashMap() }[data.target] = data
                    }
                }
            }
        }
        res.map { (cat, items) -> ItemCategory(cat, items.values.toList()) }
    }

    override suspend fun fetchSearch(query: String, category: Int): Errorable<List<ItemData>> = errorable {
        val res = ArrayList<ItemData>()
        val filters = filters[category] ?: return@errorable emptyList()
        catching {
            val search = search(query, filters)
            search.relatedItems.forEach {
                convertItem(it)?.let { data ->
                    if (data.category() == category) {
                        res.add(data)
                    }
                }
            }
        }
        res
    }

    override suspend fun fetchSuggestions(query: String): Errorable<List<String>> {
        return try {
            Errorable(service.suggestionExtractor.suggestionList(query))
        } catch (ex: Exception) {
            Errorable(emptyList(), listOf(ex))
        }
    }

    override suspend fun fetchStream(
        context: Context,
        url: String,
        sources: DataSources
    ): FetchedStreamData {
        val info = StreamInfo.getInfo(service, url)
        val stream = info.asData()
        val artRequest = ImageRequest.Builder(context)
            .data(info.thumbnailUrl)

        if (info.streamType.live) {
            fun create(factory: MediaSource.Factory, url: String) = FetchedStreamData(
                stream, artRequest,
                factory.createMediaSource(
                    MediaItem.Builder()
                        .setUri(url)
                        .setLiveConfiguration(LIVE_CONFIG)
                        .build()))

            return if (info.hlsUrl.isNotEmpty()) create(sources.hlsSourceFactory, info.hlsUrl)
            else create(sources.dashSourceFactory, info.dashMpdUrl)
        }

        val streams = info.audioStreams
        if (streams.isEmpty())
            throw IllegalStateException("No audio streams found")

        // TODO highest quality or lowest data
        val best = when (val quality = STREAM_QUALITY_QUALITY /*Prefs.streamQuality(context)*/) {
            STREAM_QUALITY_QUALITY -> highestQuality(streams)
            STREAM_QUALITY_EFFICIENCY -> mostEfficient(streams)
            else -> throw IllegalStateException("Unknown stream quality '$quality'")
        }
        Log.d(TAG, "Fetched stream at ${best.averageBitrate} kbps")

        return FetchedStreamData(stream, artRequest,
            sources.progressiveSourceFactory.createMediaSource(
                MediaItem.Builder()
                    .setUri(best.url)
                    .build()))
    }

    override suspend fun fetchStreamList(
        context: Context,
        url: String
    ): Errorable<List<StreamData>> {
        val info = PlaylistInfo.getInfo(service, url)
        val items = HashMap<Int, StreamData>()
        val errors = ArrayList<Throwable>()
        supervisorScope {
            info.relatedItems.map { item -> async {
                item.asData() }
            }.mapIndexed { index, job ->
                try {
                    items[index] = job.await()
                } catch (ex: Exception) {
                    errors.add(ex)
                }
            }
        }
        return Errorable(items.toSortedMap(Comparator.comparingInt { it }).values.toList(), errors)
    }

    companion object {
        // 0 = lowest quality
        val FORMAT_HIGHEST_QUALITY = listOf(MediaFormat.MP3, MediaFormat.WEBMA, MediaFormat.M4A)
        // 0 = most efficient
        val FORMAT_MOST_EFFICIENT = listOf(MediaFormat.WEBMA, MediaFormat.M4A, MediaFormat.MP3)
        const val STREAM_EDGE_GAP = 10_000L

        val LIVE_CONFIG = MediaItem.LiveConfiguration.Builder()
            .setTargetOffsetMs(STREAM_EDGE_GAP)
            .build()

        fun compareByBitrate(a: AudioStream, b: AudioStream, ranks: List<MediaFormat>) =
            if (a.averageBitrate < b.averageBitrate) -1
            else if (a.averageBitrate > b.averageBitrate) 1
            // if they have the same bitrate, sort by our format ranking
            else ranks.indexOf(a.getFormat()) - ranks.indexOf(b.getFormat())

        fun highestQuality(streams: List<AudioStream>): AudioStream {
            return streams.stream()
                .max { a, b -> compareByBitrate(a, b, FORMAT_HIGHEST_QUALITY) }
                .get()
        }

        fun mostEfficient(streams: List<AudioStream>): AudioStream {
            return streams.stream()
                .max { a, b -> -compareByBitrate(a, b, FORMAT_MOST_EFFICIENT) }
                .get()
        }
    }
}

private fun asSongArtist(name: String): String? {
    YT_SONG_SUFFIXES.forEach { suffix ->
        if (name.endsWith(suffix)) {
            return name.substring(0, name.length - suffix.length)
        }
    }
    return null
}
