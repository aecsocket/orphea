package com.github.aecsocket.orphea

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import androidx.compose.material.ScaffoldState
import androidx.compose.material.SnackbarResult
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import com.github.aecsocket.orphea.route.InvalidServiceException
import kotlinx.parcelize.Parcelize
import org.schabi.newpipe.extractor.exceptions.*
import java.io.IOException
import java.io.PrintWriter
import java.io.StringWriter

data class Errorable<T>(
    val result: T,
    val errors: List<Throwable> = emptyList()
) {
    interface Scope {
        fun catching(action: () -> Unit)
    }
}

fun <T> errorable(action: Errorable.Scope.() -> T): Errorable<T> {
    val errors = ArrayList<Throwable>()
    val result = action(object : Errorable.Scope {
        override fun catching(action: () -> Unit) {
            try {
                action()
            } catch (ex: Exception) {
                errors.add(ex)
            }
        }
    })
    return Errorable(result, errors)
}

@Parcelize
data class ErrorInfo(
    val parts: List<ErrorPart>
) : Parcelable

@Parcelize
data class ErrorPart(
    val message: String,
    val detail: String
) : Parcelable

@Composable
fun Throwable.errorPart() = ErrorPart(
    errorMessage(this),
    StringWriter().use { writer ->
        printStackTrace(PrintWriter(writer))
        writer.buffer.toString()
            .split('\n')
            .filter { it.isNotEmpty() }
            .joinToString("\n")
    }
)

@Composable
fun Throwable.errorInfo() = ErrorInfo(listOf(errorPart()))

@Composable
fun List<Throwable>.errorInfo() = ErrorInfo(map { it.errorPart() })

@Composable
fun errorMessage(ex: Throwable): String {
    return stringResource(when (ex) {
        is InvalidServiceException -> R.string.error_invalid_service
        is NoNetworkException -> R.string.error_no_network
        is IOException -> R.string.error_network
        is AgeRestrictedContentException -> R.string.error_age_restricted_content
        is GeographicRestrictionException -> R.string.error_georestricted_content
        is PaidContentException -> R.string.error_paid_content
        is PrivateContentException -> R.string.error_private_content
        is SoundCloudGoPlusContentException -> R.string.error_soundcloud_go_plus_content
        is YoutubeMusicPremiumContentException -> R.string.error_youtube_music_premium_content
        is ContentNotAvailableException -> R.string.error_content_not_available
        is ContentNotSupportedException -> R.string.error_content_not_supported
        else -> if (ex.localizedMessage == null) R.string.error_generic else R.string.error_generic_message
    }, (ex.localizedMessage as Any?).toString())
}

fun errorActivityIntent(context: Context, error: ErrorInfo) = Intent(context, ErrorActivity::class.java)
    .putExtra(ERROR_INFO, error)

@Composable
fun ErrorSnackbar(scaffold: ScaffoldState, message: Int, error: ErrorInfo) {
    val context = LocalContext.current
    val messageText = stringResource(message)
    val details = stringResource(R.string.details)
    LaunchedEffect(scaffold.snackbarHostState) {
        if (scaffold.snackbarHostState.showSnackbar(messageText, details) == SnackbarResult.ActionPerformed) {
            context.startActivity(errorActivityIntent(context, error))
        }
    }
}
