package com.github.aecsocket.orphea.media

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.github.aecsocket.orphea.Errorable
import com.github.aecsocket.orphea.R
import com.github.aecsocket.orphea.errorable
import com.github.aecsocket.orphea.player.DataSources
import com.github.aecsocket.orphea.player.FetchedStreamData
import org.schabi.newpipe.extractor.NewPipe
import org.schabi.newpipe.extractor.ServiceList
import org.schabi.newpipe.extractor.exceptions.ExtractionException
import org.schabi.newpipe.extractor.services.bandcamp.BandcampService
import org.schabi.newpipe.extractor.services.soundcloud.SoundcloudService

interface DataService {
    @Composable fun name(): String
    val couldBeDefault: Boolean

    suspend fun fetchUrl(url: String): ItemData?

    suspend fun fetchSearch(query: String): Errorable<List<ItemCategory>>

    suspend fun fetchSearch(query: String, category: Int): Errorable<List<ItemData>>

    suspend fun fetchSuggestions(query: String): Errorable<List<String>>

    suspend fun fetchStream(context: Context, url: String, sources: DataSources): FetchedStreamData

    suspend fun fetchStreamList(context: Context, url: String): Errorable<List<StreamData>>
}

object DummyDataService : DataService {
    @Composable override fun name(): String = "dummy"
    override val couldBeDefault: Boolean
        get() = false

    override suspend fun fetchUrl(url: String): ItemData? = null

    override suspend fun fetchSearch(query: String): Errorable<List<ItemCategory>> =
        Errorable(emptyList())

    override suspend fun fetchSearch(query: String, category: Int): Errorable<List<ItemData>> =
        Errorable(emptyList())

    override suspend fun fetchSuggestions(query: String): Errorable<List<String>> =
        Errorable(emptyList())

    override suspend fun fetchStream(
        context: Context,
        url: String,
        sources: DataSources
    ): FetchedStreamData {
        TODO("Not yet implemented")
    }

    override suspend fun fetchStreamList(
        context: Context,
        url: String
    ): Errorable<List<StreamData>> = Errorable(emptyList())
}

object LocalDataService : DataService {
    @Composable override fun name(): String = stringResource(R.string.local)
    override val couldBeDefault
        get() = false

    override suspend fun fetchUrl(url: String): ItemData? = null

    override suspend fun fetchSearch(query: String) = errorable<List<ItemCategory>> {
        emptyList()
    }

    override suspend fun fetchSearch(query: String, category: Int) = errorable<List<ItemData>> {
        emptyList()
    }

    override suspend fun fetchSuggestions(query: String): Errorable<List<String>> {
        return Errorable(emptyList())
    }

    override suspend fun fetchStream(
        context: Context,
        url: String,
        sources: DataSources
    ): FetchedStreamData {
        TODO("Not yet implemented")
    }

    override suspend fun fetchStreamList(
        context: Context,
        url: String
    ): Errorable<List<StreamData>> = Errorable(emptyList())
}

object SpotifyDataService : DataService {
    @Composable override fun name(): String = stringResource(R.string.spotify)
    override val couldBeDefault
        get() = true

    override suspend fun fetchUrl(url: String): ItemData? = null

    override suspend fun fetchSearch(query: String) = errorable<List<ItemCategory>> {
        emptyList()
    }

    override suspend fun fetchSearch(query: String, category: Int) = errorable<List<ItemData>> {
        emptyList()
    }

    override suspend fun fetchSuggestions(query: String): Errorable<List<String>> {
        return Errorable(emptyList())
    }

    override suspend fun fetchStream(
        context: Context,
        url: String,
        sources: DataSources
    ): FetchedStreamData {
        TODO("Not yet implemented")
    }

    override suspend fun fetchStreamList(
        context: Context,
        url: String
    ): Errorable<List<StreamData>> = Errorable(emptyList())
}

object DataServices {
    val ByNewPipe = ServiceList.all().map {
        val streams = listOf("videos", "sepia_video", "music_videos", "tracks", "music_songs")
        val uploaders = listOf("channels", "users", "music_artists")
        val lists = listOf("playlists", "music_playlists", "music_albums")

        NewPipeDataService(it, when (it) {
            is SoundcloudService, is BandcampService -> DataItemTypes.Songs
            else -> DataItemTypes.Videos
        }, mapOf(
            CATEGORY_SONGS to streams,
            CATEGORY_VIDEOS to streams,
            CATEGORY_ARTISTS to uploaders,
            CATEGORY_CHANNELS to uploaders,
            CATEGORY_ALBUMS to lists,
            CATEGORY_PLAYLISTS to lists
        )) }

    val All = listOf(
        //LocalDataService,
        NewPipeDataService(ServiceList.YouTube, DataItemTypes.Songs, mapOf(
            CATEGORY_SONGS to listOf("music_songs"),
            CATEGORY_ARTISTS to listOf("music_artists"),
            CATEGORY_ALBUMS to listOf("music_albums"),
            CATEGORY_PLAYLISTS to listOf("music_playlists")
        ), R.string.yt_music),
        //SpotifyDataService
    ) + ByNewPipe

    fun byUrl(url: String): DataService? {
        return try {
            val newPipe = NewPipe.getServiceByUrl(url)
            ByNewPipe[newPipe.serviceId]
                // every NewPipe service should be mapped
                ?: throw IllegalStateException("NewPipe service ${newPipe.serviceInfo.name} does not have corresponding DataService")
        } catch (ex: ExtractionException) {
            null
        }
    }
}
