package com.github.aecsocket.orphea

import android.app.Application
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationManagerCompat
import com.github.aecsocket.orphea.player.MediaPlayer
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.schabi.newpipe.extractor.NewPipe
import org.schabi.newpipe.extractor.downloader.Downloader
import org.schabi.newpipe.extractor.downloader.Request
import org.schabi.newpipe.extractor.downloader.Response
import org.schabi.newpipe.extractor.exceptions.ReCaptchaException
import org.schabi.newpipe.extractor.localization.ContentCountry
import org.schabi.newpipe.extractor.localization.Localization
import java.util.*
import java.util.concurrent.TimeUnit

const val USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0"
const val PKG = BuildConfig.APPLICATION_ID
const val TAG = "Orphea"
const val CHANNEL_MEDIA = "media"
const val CHANNEL_ERROR = "error"

class App : Application() {
    lateinit var player: MediaPlayer

    override fun onCreate() {
        super.onCreate()
        player = MediaPlayer(this)
        val downloader = object : Downloader() {
            val client = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

            override fun execute(req: Request): Response {
                val url = req.url()
                val reqBuilder = okhttp3.Request.Builder()
                    .method(req.httpMethod(), req.dataToSend()?.let {
                        it.toRequestBody(
                            null,
                            0,
                            it.size
                        )
                    })
                    .url(url)
                    .addHeader("User-Agent", USER_AGENT)

                req.headers().forEach { (name, values) ->
                    if (values.size > 1) {
                        reqBuilder.removeHeader(name)
                        values.forEach { reqBuilder.addHeader(name, it) }
                    } else {
                        reqBuilder.header(name, values[0])
                    }
                }

                val rsp = client.newCall(reqBuilder.build()).execute()
                if (rsp.code == 429) {
                    rsp.close()
                    throw ReCaptchaException("reCaptcha Challenge requested", url)
                }

                return Response(
                    rsp.code, rsp.message, rsp.headers.toMultimap(),
                    rsp.body?.string(), rsp.request.url.toString())
            }
        }

        NewPipe.init(downloader,
            Localization.fromLocale(Locale.getDefault()),
            ContentCountry.DEFAULT)

        NotificationManagerCompat.from(this).apply {
            fun createChannel(channel: String, action: NotificationChannelCompat.Builder.() -> Unit) {
                val builder = NotificationChannelCompat.Builder(channel, NotificationManagerCompat.IMPORTANCE_LOW)
                action(builder)
                createNotificationChannel(builder.build())
            }

            createChannel(CHANNEL_MEDIA) {
                setName(getString(R.string.notif_media))
                setDescription(getString(R.string.notif_media_desc))
            }

            createChannel(CHANNEL_ERROR) {
                setName(getString(R.string.notif_error))
                setDescription(getString(R.string.notif_error_desc))
            }
        }
    }
}

@Composable
fun formatTime(ms: Long): String {
    val hours = ms / (1000 * 60 * 60)
    val minutes = (ms % (1000 * 60 * 60)) / (1000 * 60)
    val seconds = (ms % (1000 * 60)) / 1000

    return if (hours > 0) stringResource(R.string.time_hours, hours, minutes, seconds)
    else stringResource(R.string.time_minutes, minutes, seconds)
}

@Composable
fun formatOptTime(ms: Long?) =
    ms?.let { formatTime(it) } ?: stringResource(R.string.time_unknown)
