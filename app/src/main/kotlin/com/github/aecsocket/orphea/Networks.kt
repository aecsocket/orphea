package com.github.aecsocket.orphea

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities

object NoNetworkException : RuntimeException()

fun hasNetwork(context: Context): Boolean {
    val connManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    with(connManager.getNetworkCapabilities(
        connManager.activeNetwork ?: return false) ?: return false) {
        return when {
            hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}
