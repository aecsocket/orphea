package com.github.aecsocket.orphea

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.github.aecsocket.orphea.media.SquareIconComponent
import com.github.aecsocket.orphea.media.StreamComponent
import com.github.aecsocket.orphea.media.StreamData
import com.github.aecsocket.orphea.media.StreamListData
import com.github.aecsocket.orphea.player.*
import com.github.aecsocket.orphea.route.SearchPage
import com.github.aecsocket.orphea.ui.theme.OrpheaTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            OrpheaTheme {
                Surface(modifier = Modifier.fillMaxSize()) {
                    Main()
                }
            }
        }
    }
}

data class TabData(
    val icon: Int,
    val description: Int,
    val content: @Composable BoxScope.(MediaPlayer) -> Unit
)

val Tabs = listOf(
    TabData(R.drawable.ic_search, R.string.search) { player ->
        val scope = rememberCoroutineScope()
        val context = LocalContext.current
        val elements by player.queue.elements.collectAsState()
        SearchPage(
            added = { data ->
                when (data) {
                    is StreamData -> {
                        elements.indexOfStream(data) != -1
                    }
                    else -> false // todo
                }
            },
            onClickItem = { data ->
                when (data) {
                    is StreamData -> {
                        player.queue.clear()
                        player.queue.add(data)
                        player.play()
                    }
                    //is StreamListData -> player.queue.add(StreamListElement(it))
                    else -> {} // todo
                }
            },
            onAddItem = { data ->
                when (data) {
                    is StreamData -> {
                        if (player.queue.add(data)) {
                            player.play()
                        }
                    }
                    is StreamListData -> {
                        scope.launch(Dispatchers.IO) {
                            val (streams, errors) = data.fetchStreamList(context)
                            // todo error handling
                            scope.launch(Dispatchers.Main) {
                                if (player.queue.addAll(streams)) {
                                    player.play()
                                }
                            }
                        }
                    }
                    else -> {} // todo
                }
            },
            onRemoveItem = { data ->
                when (data) {
                    is StreamData -> {
                        player.queue.remove(data)
                    }
                    else -> {} // todo
                }
            }
        )
    },
    TabData(R.drawable.ic_home, R.string.home) {
        Text(text = "Home", modifier = Modifier.padding(all = 8.dp))
    },
    TabData(R.drawable.ic_library, R.string.library) {
        Text(text = "Library", modifier = Modifier.padding(all = 8.dp))
    },
)

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun Main() {
    val context = LocalContext.current
    var tabIndex by remember { mutableStateOf(0) }
    val player = context.player
    val elements by player.queue.elements.collectAsState()
    val stream by player.queue.stream.collectAsState()
    val index by player.queue.index.collectAsState()
    val state by player.state.collectAsState()
    val position by player.position.collectAsState()
    val duration by player.duration.collectAsState()
    val scaffoldState = rememberBottomSheetScaffoldState()

    if (stream == null) {
        LaunchedEffect(scaffoldState) {
            scaffoldState.bottomSheetState.collapse()
        }
    }

    Column {
        val maxPeekHeight = 72.dp
        val peekHeight = animateDpAsState(if (stream == null) 0.dp else maxPeekHeight)
        BottomSheetScaffold(
            scaffoldState = scaffoldState,
            sheetContent = {
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .height(maxPeekHeight)
                ) {
                    Row(modifier = Modifier
                        .weight(1f)
                        .clickable {
                            context.startActivity(
                                Intent(
                                    context,
                                    StreamDetailsActivity::class.java
                                )
                            )
                        }
                    ) {
                        stream?.let { element ->
                            SquareIconComponent(
                                artUrl = element.artUrl,
                                artDescription = element.name,
                                primary = element.name,
                                secondary = element.uploader,
                                modifier = Modifier
                                    .weight(1f)
                                    .align(Alignment.CenterVertically)
                            )
                        }

                        val playing = when (state) {
                            STATE_PLAYING, STATE_FETCHING -> true
                            else -> false
                        }
                        IconButton(
                            modifier = Modifier.align(Alignment.CenterVertically),
                            onClick = { if (playing) player.pause() else player.play() },
                        ) {
                            // todo animate icon
                            IconImage(
                                painter = painterResource(if (playing) R.drawable.ic_pause else R.drawable.ic_play),
                                contentDescription = stringResource(if (playing) R.string.pause else R.string.play),
                            )
                        }

                        IconButton(
                            modifier = Modifier.align(Alignment.CenterVertically),
                            onClick = { player.skipNext() },
                        ) {
                            IconImage(
                                painter = painterResource(R.drawable.ic_skip_next),
                                contentDescription = stringResource(R.string.skip_next)
                            )
                        }
                    }

                    LinearProgressIndicator(
                        progress = position.position.toFloat() / duration,
                        modifier = Modifier.fillMaxWidth(),
                    )
                }

                Surface(
                    elevation = 4.dp,
                    modifier = Modifier.fillMaxSize(),
                ) {
                    LazyColumn {
                        items(elements.size, { elements[it].id }) { elemIdx ->
                            val child = elements[elemIdx]
                            val dismissState = rememberDismissState()
                            if (dismissState.isDismissed(DismissDirection.StartToEnd)) {
                                player.queue.remove(elemIdx)
                            }

                            SwipeToDismiss(
                                state = dismissState,
                                directions = setOf(DismissDirection.StartToEnd),
                                dismissThresholds = { FractionalThreshold(0.5f) },
                                background = {}
                            ) {
                                val selected = elemIdx == index
                                val surfaceColor by animateColorAsState(
                                    (if (selected) MaterialTheme.colors.primary.copy(0.1f)
                                    else Color.Transparent)
                                )
                                Box(
                                    modifier = Modifier.background(surfaceColor)
                                ) {
                                    StreamComponent(
                                        data = child.stream,
                                        modifier = if (selected) Modifier else Modifier.clickable {
                                            player.queue.setIndex(elemIdx)
                                        }
                                    )
                                }
                            }
                        }
                    }
                }
            },
            sheetPeekHeight = peekHeight.value
        ) {
            Column {
                Box(
                    content = { Tabs[tabIndex].content(this, player) },
                    modifier = Modifier
                        .padding(it)
                        .weight(1f)
                        .fillMaxWidth()
                )
            }
        }

        /*Surface(
            elevation = 4.dp
        ) {
            TabRow(
                selectedTabIndex = tabIndex,
                contentColor = MaterialTheme.colors.primary,
                backgroundColor = MaterialTheme.colors.surface,
            ) {
                Tabs.forEachIndexed { index, tab ->
                    val selected = tabIndex == index
                    Tab(
                        selected = selected,
                        onClick = { tabIndex = index },
                        icon = { Image(
                            painter = painterResource(tab.icon),
                            contentDescription = stringResource(tab.description),
                            colorFilter = ColorFilter.tint(if (selected) MaterialTheme.colors.primary
                            else MaterialTheme.colors.onSurface)
                        ) },
                    )
                }
            }
        }*/
    }
}

@Preview("Light Mode")
@Preview("Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewMain() {
    OrpheaPreview(Modifier.fillMaxSize()) {
        Main()
    }
}
