package com.github.aecsocket.orphea

import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import com.github.aecsocket.orphea.ui.theme.OrpheaTheme
import kotlinx.coroutines.delay

@Composable
fun OrpheaPreview(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    OrpheaTheme {
        Surface(color = MaterialTheme.colors.background, modifier = modifier) {
            Box {
                content()
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ChipDefaults.chipColorsSelected() = chipColors(
    contentColor = MaterialTheme.colors.primary,
    backgroundColor = MaterialTheme.colors.primary.copy(0.1f),
)

@Composable
fun IconImage(
    painter: Painter,
    contentDescription: String?,
    modifier: Modifier = Modifier,
    alignment: Alignment = Alignment.Center,
    contentScale: ContentScale = ContentScale.Fit,
    alpha: Float = DefaultAlpha
) {
    Image(
        painter = painter,
        contentDescription = contentDescription,
        modifier = modifier,
        alignment = alignment,
        contentScale = contentScale,
        alpha = alpha,
        colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
    )
}

// https://stackoverflow.com/questions/68974245/marquee-text-effect-in-jetpack-compose
@Composable
fun Marquee(
    modifier: Modifier = Modifier,
    duration: Int,
    repeatAfter: Int,
    content: @Composable () -> Unit
) {
    val scrollState = rememberScrollState()
    var shouldAnimate by remember { mutableStateOf(true) }
    LaunchedEffect(key1 = shouldAnimate){
        scrollState.animateScrollTo(
            scrollState.maxValue,
            animationSpec = tween(duration, repeatAfter, easing = CubicBezierEasing(0f,0f,0f,0f))
        )
        delay(repeatAfter.toLong())
        scrollState.scrollTo(0)
        shouldAnimate = !shouldAnimate
    }
    Box(modifier.horizontalScroll(scrollState, false)) {
        content()
    }
}
