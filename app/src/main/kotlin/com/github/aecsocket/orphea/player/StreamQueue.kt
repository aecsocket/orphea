package com.github.aecsocket.orphea.player

import com.github.aecsocket.orphea.media.StreamData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

const val INDEX_NONE = -1

data class QueueElement(
    val stream: StreamData,
    val id: Long
)

class StreamQueue {
    private var nextId = 0L
    private fun nextId() = nextId++

    private val _elements = MutableStateFlow<List<QueueElement>>(emptyList())
    val elements: StateFlow<List<QueueElement>> get() = _elements

    private val _index = MutableStateFlow(INDEX_NONE)
    val index: StateFlow<Int> get() = _index

    private val _stream = MutableStateFlow<StreamData?>(null)
    val stream: StateFlow<StreamData?> get() = _stream

    // internal; used for operations which should be safe
    // and we *want* to get a stack trace if we do something stupid
    private fun forceIndex(index: Int) {
        _index.value = index
        _stream.value = _elements.value[index].stream
    }

    fun setIndex(index: Int) {
        if (index < 0 || index >= _elements.value.size)
            return
        forceIndex(index)
    }

    // returns if we just initialized the queue from this action
    fun addAll(streams: Collection<StreamData>): Boolean {
        val newElements = _elements.value.toMutableList()
        val wasEmpty = newElements.isEmpty()
        streams.forEach { stream ->
            if (_elements.value.none { it.stream.same(stream) }) {
                newElements.add(QueueElement(stream, nextId()))
            }
        }
        _elements.value = newElements
        if (wasEmpty && newElements.isNotEmpty()) {
            forceIndex(0)
            return true
        }
        return false
    }

    fun add(stream: StreamData) = addAll(setOf(stream))

    fun remove(index: Int) {
        if (index < 0 || index >= _elements.value.size)
            return
        val newElements = _elements.value.toMutableList().apply { removeAt(index) }
        _elements.value = newElements
        if (newElements.isEmpty()) {
            clear()
            return
        }

        if (index == _index.value) {
            // we just removed our currently playing element
            if (index == newElements.size) {
                // if we're at the last element, go back
                forceIndex(index - 1)
            } else {
                // play the next one
                forceIndex(index)
            }
        } else if (index < _index.value) {
            // move the index down but don't update our currently playing element
            _index.value--
        }
    }

    fun remove(stream: StreamData) {
        val index = _elements.value.indexOfStream(stream)
        if (index > -1) {
            remove(index)
        }
    }

    fun clear() {
        _elements.value = emptyList()
        _index.value = INDEX_NONE
        _stream.value = null
    }

    fun skipNext(): Boolean {
        if (_index.value < _elements.value.size - 1) {
            forceIndex(_index.value + 1)
            return true
        }
        return false
    }

    fun skipPrevious(): Boolean {
        if (_index.value > 0) {
            forceIndex(_index.value - 1)
            return true
        }
        return false
    }
}

fun List<QueueElement>.indexOfStream(stream: StreamData): Int =
    indexOfFirst { it.stream.same(stream) }
