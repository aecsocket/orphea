package com.github.aecsocket.orphea.media

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.github.aecsocket.orphea.Errorable
import com.github.aecsocket.orphea.R
import com.github.aecsocket.orphea.formatTime
import com.github.aecsocket.orphea.media.DummyDataService.fetchStreamList
import com.github.aecsocket.orphea.player.DataSources
import com.github.aecsocket.orphea.player.FetchedStreamData
import kotlinx.coroutines.CoroutineScope
import org.schabi.newpipe.extractor.stream.StreamType

const val STREAM_SONG = 0
const val STREAM_VIDEO = 1
const val UPLOADER_ARTIST = 0
const val UPLOADER_CHANNEL = 1
const val STREAM_LIST_ALBUM = 0
const val STREAM_LIST_PLAYLIST = 1

sealed interface ItemData {
    val target: String
    val service: DataService

    fun same(other: ItemData) = target == other.target
}

data class StreamData(
    override val target: String,
    override val service: DataService,
    val name: String,
    val uploader: String,
    val artUrl: String,
    val type: Int,
    val duration: Long,
    val streamType: StreamType,
) : ItemData {
    suspend fun fetchSource(context: Context, sources: DataSources): FetchedStreamData =
        service.fetchStream(context, target, sources)

    override fun toString() =
        "StreamData($name - $uploader)"
}

data class UploaderData(
    override val target: String,
    override val service: DataService,
    val name: String,
    val artUrl: String,
    val type: Int,
) : ItemData

data class StreamListData(
    override val target: String,
    override val service: DataService,
    val name: String,
    val uploader: String,
    val artUrl: String,
    val type: Int,
    val size: Long?,
) : ItemData {
    suspend fun fetchStreamList(context: Context): Errorable<List<StreamData>> =
        service.fetchStreamList(context, target)
}

@Composable
fun DataComponent(
    artUrl: String,
    artDescription: String,
    artModifier: Modifier,
    primary: String,
    secondary: String,
    modifier: Modifier = Modifier,
) {
    val height = 48.dp
    Row(modifier = modifier
        .fillMaxWidth()
        .padding(vertical = 4.dp)
        .height(height)
    ) {
        AsyncImage(
            model = artUrl,
            contentDescription = artDescription,
            contentScale = ContentScale.Crop,
            alignment = Alignment.Center,
            modifier = Modifier
                .padding(horizontal = 8.dp)
                .width(height)
                .height(height)
                .then(artModifier),
        )

        Spacer(Modifier.width(4.dp))

        Box(
            Modifier
                .align(Alignment.CenterVertically)
                .padding(end = 8.dp)
                .fillMaxWidth()
        ) {
            Column {
                Text(
                    text = primary,
                    color = MaterialTheme.colors.onSurface,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                )
                Text(
                    text = secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                )
            }
        }
    }
}

@Composable
fun SquareIconComponent(
    artUrl: String,
    artDescription: String,
    primary: String,
    secondary: String,
    modifier: Modifier = Modifier
) {
    DataComponent(
        artUrl = artUrl,
        artDescription = artDescription,
        artModifier = Modifier.clip(MaterialTheme.shapes.medium),
        primary = primary,
        secondary = secondary,
        modifier = modifier
    )
}

@Composable
fun CircleIconComponent(
    artUrl: String,
    artDescription: String,
    primary: String,
    secondary: String,
    modifier: Modifier = Modifier
) {
    DataComponent(
        artUrl = artUrl,
        artDescription = artDescription,
        artModifier = Modifier.clip(CircleShape),
        primary = primary,
        secondary = secondary,
        modifier = modifier
    )
}

@Composable
fun StreamComponent(
    data: StreamData,
    modifier: Modifier = Modifier,
) {
    SquareIconComponent(
        artUrl = data.artUrl,
        artDescription = data.name,
        primary = data.name,
        secondary = stringResource(when (data.type) {
            STREAM_SONG -> R.string.info_song
            STREAM_VIDEO -> R.string.info_video
            else -> throw IllegalArgumentException("unknown stream item type ${data.type}")
        }, formatTime(data.duration), data.uploader),
        modifier = modifier,
    )
}

@Composable
fun UploaderComponent(
    data: UploaderData,
    modifier: Modifier = Modifier,
) {
    CircleIconComponent(
        artUrl = data.artUrl,
        artDescription = data.name,
        primary = data.name,
        secondary = stringResource(when (data.type) {
            UPLOADER_ARTIST -> R.string.info_artist
            UPLOADER_CHANNEL -> R.string.info_channel
            else -> throw IllegalArgumentException("unknown uploader item type ${data.type}")
        }),
        modifier = modifier,
    )
}

@Composable
fun StreamListComponent(
    data: StreamListData,
    modifier: Modifier = Modifier,
) {
    SquareIconComponent(
        artUrl = data.artUrl,
        artDescription = data.name,
        primary = data.name,
        secondary = stringResource(when (data.type) {
            STREAM_LIST_ALBUM -> if (data.size == null) R.string.info_album_no_count
                else R.string.info_album_count
            STREAM_LIST_PLAYLIST -> if (data.size == null) R.string.info_playlist_no_count
                else R.string.info_playlist_count
            else -> throw IllegalArgumentException("unknown stream list item type ${data.type}")
        }, data.uploader, data.size ?: 0),
        modifier = modifier,
    )
}

@Composable
fun ItemComponent(
    data: ItemData,
    modifier: Modifier
) {
    when (data) {
        is StreamData -> StreamComponent(data = data, modifier = modifier)
        is UploaderData -> UploaderComponent(data = data, modifier = modifier)
        is StreamListData -> StreamListComponent(data = data, modifier = modifier)
    }
}

@Preview
@Composable
fun PreviewStreamComponent() {
    StreamComponent(data = StreamData(
        "..",
        DummyDataService,
        "Some song",
        "Death Grips",
        "https://upload.wikimedia.org/wikipedia/commons/6/64/MF_Doom_-_Hultsfred_2011_%28cropped%29.jpg",
        STREAM_SONG,
        1000 * 60 * 4,
        StreamType.VIDEO_STREAM)
    )
}

@Preview
@Composable
fun PreviewUploaderComponent() {
    UploaderComponent(data = UploaderData(
        "..",
        DummyDataService,
        "MF DOOM",
        "https://upload.wikimedia.org/wikipedia/commons/6/64/MF_Doom_-_Hultsfred_2011_%28cropped%29.jpg",
        UPLOADER_ARTIST)
    )
}


